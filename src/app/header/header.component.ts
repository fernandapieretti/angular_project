import { Component, OnInit } from '@angular/core';
import { HostListener, Inject } from "@angular/core";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  constructor() { 
  }

  ngOnInit() {
  }

  public headerClass = "";
  public isCollapsed = true;

  @HostListener("window:scroll", [])
  onWindowScroll() {

    const number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (number > 200) {
      this.headerClass = "bg-dark";
    } else {
      this.headerClass = "";
    }

  }

}
