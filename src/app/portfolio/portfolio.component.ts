import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../projects.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.sass']
})

export class PortfolioComponent implements OnInit {

  constructor(private _projectsService: ProjectsService) { }

  public projects = [];

  filterProjects(filter) {
    this.projects = this._projectsService.getProjects().filter((project) => project.type === filter);
  }

  ngOnInit() {
    this.filterProjects('destaques');
  }

}
