import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor() { }

  getProjects() {
    return [
      {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/senai/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-95.jpg',
        'name': 'Senai',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'destaques',
        'url': 'http://www.freelancehtmlcss.com/freelance/avalue/sindicario/v1-1.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-109.jpg',
        'name': 'Sindicato dos Bancários de CG',
        'technologies': 'HTML5; CSS3; Bootstrap 4; jQuery'
      }, {
        'type': 'destaques',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/simeam/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-120.jpg',
        'name': 'Sindicato dos Médicos do AM',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/controllife/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-121.jpg',
        'name': 'Clube de Descontos Control Life',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'destaques',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardomazzoni/lira/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-111.jpg',
        'name': 'Livraria Lira',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/djd/pagina-1.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-122.jpg',
        'name': 'Diretoria de Justiça e Disciplina',
        'technologies': 'HTML5; CSS3; Bootstrap 4; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/revista/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-113.jpg',
        'name': 'Marketing Cultural',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/tgs/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-123.jpg',
        'name': 'Transglobal Serviços',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/insetmax/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-112.jpg',
        'name': 'Insetimax',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/avalue/sinergia',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-110.jpg',
        'name': 'Sinergia',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/clubinho',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-107.png',
        'name': 'Clubinho de Leitura',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/sanpaolo',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-108.png',
        'name': 'San Paolo Gelateria',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/ecoclimas/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-99.jpg',
        'name': 'Ecoclimas',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/meetingfloripa/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-106.png',
        'name': 'Meeting Floripa',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/assinestore/boxdamadrinha/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-100.jpg',
        'name': 'Box da Madrinha',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/mr3/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-102.jpg',
        'name': 'MR3 Mineração',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/mercury/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-105.png',
        'name': 'Mercury',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/mercurygroup/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-129.jpg',
        'name': 'Mercury Group',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/utilitat',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-101.jpg',
        'name': 'Utilitat Presentes',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/labibs/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-94.jpg',
        'name': 'Família Labib`s',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/feirao/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-103.jpg',
        'name': 'Feirão de Veículos',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/apeam/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-104.jpg',
        'name': 'APEAM',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/top',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-96.jpg',
        'name': 'Top Uniformes',
        'technologies': 'HTML5; CSS3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/level',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-91.jpg',
        'name': 'Level Salão e Spa Urbano',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/gpoder',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-90.jpg',
        'name': 'Grupo Poder',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/rafaelnaruto/concrelaje',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-05.jpg',
        'name': 'Concrelaje - A Solução Concreta',
        'technologies': 'HTML5; CSS3; jQuery; PHP; MySQL'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/gotalent',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-16.jpg',
        'name': 'Go Talent',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.apoemafactoring.com.br/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-27.jpg',
        'name': 'Apoema Factoring',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/drgaiao',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-86.jpg',
        'name': 'Dr. Gaião',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/canigatti',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-82.jpg',
        'name': 'Canigatti',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.mobarquitetos.com.br',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-83.jpg',
        'name': 'MOB Arquitetos',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/az/faciles/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-98.jpg',
        'name': 'Faciles',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/petcafe',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-79.jpg',
        'name': 'Pet Café',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/soltek',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-03.jpg',
        'name': 'Soltek',
        'technologies': 'HTML5; CSS3; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/automasite',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/automasite.jpg',
        'name': 'Automasite',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/dorper/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/dorper.jpg',
        'name': 'Dorper',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/agenciaweb/secondsk/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-84.jpg',
        'name': 'Second Skin',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/play/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/play.jpg',
        'name': 'Play Games PS3',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/wbi/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/wbi.jpg',
        'name': 'WBI Engenharia',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/assentamais/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/assentamais.jpg',
        'name': 'Assentamais',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/rock/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/rock.jpg',
        'name': 'Rock Laser',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/rafiam/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/rafiam.jpg',
        'name': 'Rafiam Masterpack Brasil',
        'technologies': 'HTML5; CSS3; Grid 996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/grupoilha/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/danca.jpg',
        'name': 'Grupo Ilha',
        'technologies': 'HTML5; CSS3; Grid 996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/receita/claudecy-new-receita.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-85.jpg',
        'name': 'Organiza Chef',
        'technologies': 'HTML5; CSS3; Grid 996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/cachaca',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/cachaca.jpg',
        'name': 'Cachaçaria do Dedé',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/rafaelnaruto/brazilecovip',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-04.jpg',
        'name': 'Brazil Eco Vip',
        'technologies': 'HTML4; CSS; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/redeleao',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-32.jpg',
        'name': 'Rede Leão',
        'technologies': 'HTML5; CSS3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/creativea/bestbuy/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-115.jpg',
        'name': 'Best Buy',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/mai/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-116.jpg',
        'name': 'Maturana Arquitetura e Interiores',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/marinariobello/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-13.jpg',
        'name': 'Marina Rio Bello',
        'technologies': 'HTML5; CSS3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/manha/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-57.jpg',
        'name': 'Manhã Telecomunicações',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/rafaelnaruto/semanaon',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-12.jpg',
        'name': 'Semana On',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery; PHP; MySQL'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/praticandopilates',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-07.jpg',
        'name': 'Praticando Pilates',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/campofrio',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-19.jpg',
        'name': 'Campo Frio Familia',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery; PHP; MySQL'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/rafaelnaruto/bellabufala',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-02.jpg',
        'name': 'Bella Bufalla',
        'technologies': 'HTML4; CSS; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/sanete',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-26.jpg',
        'name': 'Sanete Saneamento',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/petcase/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-125.jpg',
        'name': 'Pet Case',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/novaera/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-124.jpg',
        'name': 'Nova Era Games',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/aprimore',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-60.jpg',
        'name': 'Aprimore',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/conexaocarreira/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-39.jpg',
        'name': 'Conexão Carreira',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/limaport',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-28.jpg',
        'name': 'LimaPort',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/evs',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-01.jpg',
        'name': 'Espaço Vida Pilates',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/caput/home.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-14.jpg',
        'name': 'Caput Consultoria',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/tropicalaco',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-15.jpg',
        'name': 'Tropical Aço',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/transforma/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-118.jpg',
        'name': 'Transforma Salvador',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/karolaine/_paginas/home.php',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-10.jpg',
        'name': 'Karolaine Cred',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/ceres',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-20.jpg',
        'name': 'Ceres Imóveis Rurais',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/izac/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-63.jpg',
        'name': 'José Izac de Souza',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/amazon',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-23.jpg',
        'name': 'Amazon Nemo',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/sucuri',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-24.jpg',
        'name': 'Sucuri Pesca e Aventura',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/proadvice/home.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-06.jpg',
        'name': 'Pró-Advice',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/kauthec_ws/_paginas/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-09.jpg',
        'name': 'Kauthec do Brasil',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/feirao/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-40.jpg',
        'name': 'Feirão de Pneus',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/pp/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-59.jpg',
        'name': 'Parceria Positiva',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/rio/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-58.jpg',
        'name': 'Rio Kids',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/iba',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-22.jpg',
        'name': 'IBA Igreja Batista',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/4design/html/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-56.jpg',
        'name': '4Design',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/luisalves/igreja',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-65.jpg',
        'name': 'Igreja Quadrangular',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/v8',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-34.jpg',
        'name': 'V8 Investimentos',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/centraleventos/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-88.jpg',
        'name': 'Central de Eventos',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/vipclass',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-35.jpg',
        'name': 'VIP Class',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/magloja',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-53.jpg',
        'name': 'MAG Loja',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/miss',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-54.jpg',
        'name': 'Miss Store',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/lisboutique/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-42.jpg',
        'name': 'Lis Boutique',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/jgadita/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-62.jpg',
        'name': 'JGadita',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'destaques',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/pure-pilates',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-126.jpg',
        'name': 'Pure Pilates',
        'technologies': 'HTML5; CSS3; Bootstrap 4; jQuery'
      }, {
        'type': 'destaques',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/pic2016',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-127.jpg',
        'name': 'Pilates Internacional Conference',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/viaverde/_paginas/inicial.php',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-46.jpg',
        'name': 'Via Verde',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/prontovet/pagina1.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-29.jpg',
        'name': 'Prontovet',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/gimp',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-31.jpg',
        'name': 'GIMP Controle Contabilidade',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/personality/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-45.jpg',
        'name': 'Personality Educação Profissional',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/zzag/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-47.jpg',
        'name': 'ZZAG',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/dropbag/pagina1.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-50.jpg',
        'name': 'Drop Bag',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/fitbela/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-64.jpg',
        'name': 'Fit Bela',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/webdanca/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-30.jpg',
        'name': 'Webdança',
        'technologies': 'HTML4; CSS; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.ismac.org.br',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-11.jpg',
        'name': 'ISMAC',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/bares/index-slider.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-25.jpg',
        'name': 'Os Barés',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/manauara_ws/_paginas/index.php',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-08.jpg',
        'name': 'Manauara Bulldogs',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/cmd/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-38.jpg',
        'name': 'Colégio Mãe de Deus',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/teoriaacustica/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-44.jpg',
        'name': 'Teoria acústica',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/novety/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-41.jpg',
        'name': 'Novety',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/terranossa/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-48.jpg',
        'name': 'Terra Nossa',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/wanthaigor/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-49.jpg',
        'name': 'Wantaigor',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/msa/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-55.jpg',
        'name': 'MSA Arquitetura',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/shopdope/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-117.jpg',
        'name': 'Shop do Pé',
        'technologies': 'HTML5; CSS3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/byunna',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-36.jpg',
        'name': 'ByUnna',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/clickweb/campos/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-37.jpg',
        'name': 'Campos & Carrer',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/lk/inicial.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-17.jpg',
        'name': 'Luciano Kikão',
        'technologies': 'HTML5; CSS; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/empregavoce/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-87.jpg',
        'name': 'Emprega Você',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/mycleanbox/pagina1.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-52.jpg',
        'name': 'MyCleanBox',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/agenciaweb/diskseguro/index.html',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-61.jpg',
        'name': 'Disk Seguro',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/recimat',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-66.jpg',
        'name': 'Recimat',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/portaria',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-97.jpg',
        'name': 'Portaria Prime',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/voll',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-67.jpg',
        'name': 'VOLL Pilates Group',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/agenciaweb/konforti',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-69.jpg',
        'name': 'Konforti',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/agenciaweb/glenmark',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-70.jpg',
        'name': 'Glenmark',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/flyways',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-71.jpg',
        'name': 'FlyWays',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/alves',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-72.jpg',
        'name': 'Adriana Alves Cerimonial',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/rerthy',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-73.jpg',
        'name': 'Rerthy',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/conecte/rfid/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-114.jpg',
        'name': 'RFID Technologies',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/pm',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-74.jpg',
        'name': 'Associação dos Oficiais da PM',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'destaques',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/site',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-128.jpg',
        'name': 'Polícia Militar Amazonas',
        'technologies': 'HTML5; CSS3; Bootstrap 4; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/pmam/',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-119.jpg',
        'name': 'Pré-escola e Creche Infante Tiradentes',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/boi',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-75.jpg',
        'name': 'Boi Bumbá Garantido',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'phpmysql',
        'url': 'http://www.freelancehtmlcss.com/freelance/eduardobibiano/pilatesavancado',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-89.jpg',
        'name': 'Pilates Avançado',
        'technologies': 'HTML5; CSS3; Grid996; jQuery; PHP; MySQL'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/mvg',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-76.jpg',
        'name': 'MVG',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/ht/aej',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-77.jpg',
        'name': 'A&J Treinamentos Gerenciais',
        'technologies': 'HTML5; CSS3; Bootstrap 3; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/fullscoop',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-78.jpg',
        'name': 'Full Scoop',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/oiola',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-80.jpg',
        'name': 'Oiola',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }, {
        'type': 'htmlcss',
        'url': 'http://www.freelancehtmlcss.com/freelance/kingofweb/bicocegonha',
        'img': 'http://www.freelancehtmlcss.com/img/portfolio/image-81.jpg',
        'name': 'Bico Cegonha',
        'technologies': 'HTML5; CSS3; Grid996; jQuery'
      }
    ];
  }
}
